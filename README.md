# Entity Token Block

This module allows users to create block with token value used by token module using related entity as data in layout builder.

## Usage

1. Go to entity layout edit page.
2. Click "Add block" in any section
3. Choose "Entity Token Block"
4. In the "Configure block" section, Click "Browse available tokens" link for Body
   field to add the token.
