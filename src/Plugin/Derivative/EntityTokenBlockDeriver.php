<?php

namespace Drupal\entity_token_block\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\Context\EntityContextDefinition;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides entity token block definitions for every entity.
 *
 * @internal
 *   Plugin derivers are internal.
 */
class EntityTokenBlockDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;
  use LoggerChannelTrait;

  /**
   * The entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeRepository;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Constructs new FieldBlockDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entity_type_repository
   *   The entity type repository.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The token service.
   */
  public function __construct(EntityTypeRepositoryInterface $entity_type_repository, Token $token) {
    $this->entityTypeRepository = $entity_type_repository;
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.repository'),
      $container->get('token'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $entity_type_labels = $this->entityTypeRepository->getEntityTypeLabels();
    $token_types = $this->token->getInfo()['types'];
    foreach ($token_types as $token_type => $definition) {
      if (empty($definition['needs-data']) || isset($definition['nested'])) {
        continue;
      }
      $entity_type_id = $definition['needs-data'];
      if ($entity_type_id == 'term') {
        $entity_type_id = 'taxonomy_term';
      }

      if (empty($entity_type_labels[$entity_type_id])) {
        continue;
      }
      $derivative = $base_plugin_definition;
      $derivative['admin_label'] = $entity_type_labels[$entity_type_id];

      $context_definition = EntityContextDefinition::fromEntityTypeId($entity_type_id)->setLabel($entity_type_labels[$entity_type_id]);
      $derivative['context_definitions'] = [
        'entity' => $context_definition,
      ];

      $derivative_id = $token_type . PluginBase::DERIVATIVE_SEPARATOR . $entity_type_id;
      $this->derivatives[$derivative_id] = $derivative;
    }
    return $this->derivatives;
  }

}
