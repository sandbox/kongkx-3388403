<?php

declare(strict_types = 1);

namespace Drupal\entity_token_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an entity token block block.
 *
 * @Block(
 *   id = "entity_token_block",
 *   admin_label = @Translation("Entity Token Block"),
 *   category = @Translation("Entity Token Block"),
 *   deriver = "\Drupal\entity_token_block\Plugin\Derivative\EntityTokenBlockDeriver",
 * )
 */
final class EntityTokenBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The token type .
   *
   * @var string
   */
  protected $tokenType;

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Constructs the plugin instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    private readonly Token $token,
  ) {
    [, $token_type, $entity_type_id] = explode(static::DERIVATIVE_SEPARATOR, $plugin_id, 3);
    $this->tokenType = $token_type;
    $this->entityTypeId = $entity_type_id;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('token'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'label_display' => FALSE,
      'body' => [
        'value' => $this->t(''),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => $this->configuration['body']['value'],
      '#format' => $this->configuration['body']['format'],
    ];
    $form['token_label']['#markup'] = $this->t('This field supports tokens.');
    $form['token_link'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [$this->tokenType],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->setConfigurationValue('body', $form_state->getValue('body'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $description = $this->token->replace($this->configuration['body']['value'], $this->getData());
    return [
      '#markup' => $description,
    ];
  }

  /**
   * Gets the entity that has the field.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The entity.
   */
  protected function getEntity() {
    return $this->getContextValue('entity');
  }

  /**
   *
   */
  protected function getData() {
    $entity = $this->getEntity();
    if (is_null($entity)) {
      return [];
    }
    return [
      $this->entityTypeId => $entity,
    ];
  }

}
